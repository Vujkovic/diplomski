# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import *

def takeSecond(elem):
    return elem[1];

def win(team):
    if team in ranking:
        ranking[team]+=3;
    else:
        ranking.update({team: 3});
        
def draw(team1,team2):
    if team1 in ranking:
        ranking[team1]+=1;
    else:
        ranking.update({team1: 1});
    
    if team2 in ranking:
        ranking[team2]+=1;
    else:
        ranking.update({team2: 1});
   
# Importing the dataset
dataset = pd.read_csv('results.csv')
start = 1870
startDate = date(1870,1,1);
endDate = date(1880,1,1);
X = []
Y = dataset.iloc[:, 0:5].values
Z = []

for i in range(1,16):
    for j in range(0, np.size(Y,0)):
        q = Y[j][0]
        q = q.split('-')
        d,m,y = [int(i) for i in q]
        D = date(d,m,y)
        if startDate < D and D < endDate:
            Z.append(Y[j])
    startDate = date(start + i*10, 1, 1)
    endDate = date(start + i*10 + 10, 1, 1)
    X.append(Z)
    Z = []

ranking = {}
rankingList = [];

n = 0;
for x in X:
    for i in range(0, np.size(x,0)):
        if x[i][3] > x[i][4]:
            win(x[i][1]);
        elif x[i][3] < x[i][4]:
            win(x[i][2]);
        else:
            draw(x[i][1],x[i][2])
    rankingList.append(['{}-{}'.format(start + n*10, start +n*10 + 10),ranking])
    n+=1
    ranking = {}

printList = []

n = 0
for x in rankingList:
    x[1] = {k: v for k, v in sorted(x[1].items(), key=lambda item: item[1])}
    for key, value in x[1].items():
        temp = [key,value]
        printList.append(temp)
    printList.sort(key = takeSecond,reverse = 1);
    rankingList[n][1] = printList;
    printList = []
    n += 1

    
for i in rankingList:
    print("{}{}".format(i[0], i[1][0]))