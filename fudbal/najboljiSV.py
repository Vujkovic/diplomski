# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def takeSecond(elem):
    return elem[1];

def win(team):
    if team in ranking:
        ranking[team]+=3;
    else:
        ranking.update({team: 3});
        
def draw(team1,team2):
    if team1 in ranking:
        ranking[team1]+=1;
    else:
        ranking.update({team1: 1});
    
    if team2 in ranking:
        ranking[team2]+=1;
    else:
        ranking.update({team2: 1});
   
# Importing the dataset
dataset = pd.read_csv('results.csv')
X = dataset.iloc[:, 1:5].values
ranking = {}

for match in X:
    if match[2] > match[3]:
        win(match[0]);
    elif match[2] < match[3]:
        win(match[1]);
    else:
        draw(match[0],match[1])

ranking = {k: v for k, v in sorted(ranking.items(), key=lambda item: item[1])}

rankingList = [];
for key, value in ranking.items():
    temp = [key,value]
    rankingList.append(temp)
    
rankingList.sort(key = takeSecond,reverse = 1);

for i in range(0,10):
    print(rankingList[i])