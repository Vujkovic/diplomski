# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import *
# Importing the dataset
dataset = pd.read_csv('results.csv')
start = 1870
startDate = date(1870,1,1);
endDate = date(1880,1,1);
X = []
Y = dataset.iloc[:, 0:5].values
Z = []
scoredGoalsList = []

for i in range(1,16):
    for j in range(0, np.size(Y,0)):
        q = Y[j][0]
        q = q.split('-')
        d,m,y = [int(i) for i in q]
        D = date(d,m,y)
        if startDate < D and D < endDate:
            Z.append(Y[j])
    startDate = date(start + i*10, 1, 1)
    endDate = date(start + i*10 + 10, 1, 1)
    X.append(Z)
    Z = []

n = 0;
nGoals = 0;
nMatches = 0;
br = 0
plotList = [];
my_xticks = [];
ticks = []
for x in X:
    for i in range(0, np.size(x,0)):
        nGoals += x[i][3] + x[i][4]
        nMatches += 1
    br = nGoals/nMatches
    scoredGoalsList.append('{}-{} Prosecno golova: '.format(start + n*10, start +n*10 + 10) + "%.4f" % br)
    plotList.append(br)
    my_xticks.append(start + n*10)
    ticks.append([n]);
    n+=1
    br = 0
    nGoals = 0
    nMatches = 0

for i in scoredGoalsList:
    print(i)

ticks = np.reshape(ticks,(-1,1))
plotList = np.reshape(plotList,(-1,1))

#Fitting
from sklearn.svm import SVR
regressor = SVR(kernel ='rbf')
regressor.fit(ticks, plotList)

# Visualising the SVR results
X_grid = np.arange(min(ticks), max(ticks), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.xticks(ticks,my_xticks)
plt.ylim(1,6)
plt.scatter(ticks, plotList, color = 'red')
plt.plot(X_grid, regressor.predict(X_grid), color = 'blue')
plt.show()