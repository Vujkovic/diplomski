# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
   
# Importing the dataset
dataset = pd.read_csv('results.csv')
X = dataset.iloc[:, 1:5].values

nWin = 0;
nLost = 0;
nDraw = 0;
for match in X:
    if match[2] > match[3]:
        nWin += 1
    elif match[2] < match[3]:
        nLost += 1
    else:
        nDraw += 1
  
print("Utakmica pobedjenih na domacem terenu je %.2f" % (nWin/(np.size(X, 0))* 100) + "%")  
print("Utakmica izgubljenih na domacem terenu je %.2f" % (nLost/(np.size(X, 0))* 100) + "%")  
print("Utakmica neresenih na domacem terenu je %.2f" % (nDraw/(np.size(X, 0))* 100) + "%")