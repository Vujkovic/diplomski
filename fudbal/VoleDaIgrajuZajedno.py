# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def takeSecond(elem):
    return elem[1];

def win(team):
    if team in ranking:
        ranking[team]+=3;
    else:
        ranking.update({team: 3});
        
def draw(team1,team2):
    if team1 in ranking:
        ranking[team1]+=1;
    else:
        ranking.update({team1: 1});
    
    if team2 in ranking:
        ranking[team2]+=1;
    else:
        ranking.update({team2: 1});

def worse(team1,team2):
    tf1 = False
    tf2 = False
    for i in range(0, np.size(rankingList, 0)):
        if team1 == rankingList[i][0]:
            tf1 = True
            break;
        elif team2 == rankingList[i][0]:
            tf2 = True
            break;

    if tf1:
        return team1
    else:
        return team2
# Importing the dataset
dataset = pd.read_csv('results.csv')
X = dataset.iloc[:, 1:5].values
ranking = {}

for match in X:
    if match[2] > match[3]:
        win(match[0]);
    elif match[2] < match[3]:
        win(match[1]);
    else:
        draw(match[0],match[1])

ranking = {k: v for k, v in sorted(ranking.items(), key=lambda item: item[1])}

rankingList = [];
for key, value in ranking.items():
    temp = [key,value]
    rankingList.append(temp)
    
rankingList.sort(key = takeSecond,reverse = 1);

result = []
for match in X:
    if match[2] > match[3]:
        result.append(match[0])
    elif match[2] < match[3]:
        result.append(match[1])
    else:
        result.append("Draw")

X = dataset.iloc[:, 1:3].values
result = np.reshape(result,(-1,1))
#Apyori
matches = np.append(X,result, axis = 1)

transactions = []
for i in range(0, 41540):
    transactions.append([matches[i,j] for j in range(0, 3)])

# Training Apriori on the dataset
from apyori import apriori
rulesApriori = apriori(transactions, min_support = 0.002, min_confidence = 0.1, min_lift = 3)

# Visualising the rules
rules = list(rulesApriori)

#toString
results = [list(rules[i][0]) for i in range(0,len(rules))]

for i in range(0,len(rules)):
    results[i].append("Support: " + str(rules[i][1]))
    results[i].append("Confidence: " + str(rules[i][2][0][len(rules[i][2][0])-2]))
    results[i].append("Lift: " + str(rules[i][2][0][len(rules[i][2][0])-1]))

for i in range(0, len(rules)):
    print(results[i])
