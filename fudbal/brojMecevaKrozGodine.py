# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import *
# Importing the dataset
dataset = pd.read_csv('results.csv')
start = 1870
startDate = date(1870,1,1);
endDate = date(1880,1,1);
X = []
Y = dataset.iloc[:, 0:5].values
Z = []
matchesPlayedList = []

for i in range(1,16):
    for j in range(0, np.size(Y,0)):
        q = Y[j][0]
        q = q.split('-')
        d,m,y = [int(i) for i in q]
        D = date(d,m,y)
        if startDate < D and D < endDate:
            Z.append(Y[j])
    startDate = date(start + i*10, 1, 1)
    endDate = date(start + i*10 + 10, 1, 1)
    X.append(Z)
    Z = []

n = 0;
nMatches = 0;
plotList = [];
my_xticks = [];
ticks = []
for x in X:
    nMatches = len(x)
    matchesPlayedList.append('{}-{} Broj utakmica: '.format(start + n*10, start +n*10 + 10) + "%d" % nMatches)
    plotList.append(nMatches)
    my_xticks.append(start + n*10)
    ticks.append([n]);
    n+=1
    nMatches = 0

for i in matchesPlayedList:
    print(i)

ticks = np.reshape(ticks,(-1,1))
plotList = np.reshape(plotList,(-1,1))

from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
y = sc_y.fit_transform(plotList)
X = sc_X.fit_transform(ticks)

#Fitting
from sklearn.svm import SVR
regressor = SVR(kernel ='rbf')
regressor.fit(X, y)

# Visualising the SVR results
X_grid = np.arange(min(X), max(X), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.xticks(sc_X.inverse_transform(X),my_xticks)
plt.scatter(sc_X.inverse_transform(X), sc_y.inverse_transform(y), color = 'red')
plt.plot(sc_X.inverse_transform(X), sc_y.inverse_transform(regressor.predict(X)), color = 'blue')
plt.show()