# SVR

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('spisak-toplotnih-podstanica-sa-mesecnom-potrosnjom-za-period-2016-2018-godine-sabac.csv')
X = dataset.iloc[:, 7:8].values
y = dataset.iloc[:, 9:10].values

# Taking care of missing data
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values= 0, strategy = 'mean')
X[:,:] = imputer.fit_transform(X[:,:])
y[:,:] = imputer.fit_transform(y[:,:])

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
y = sc_y.fit_transform(y)
X = sc_X.fit_transform(X)

# Fitting SVR to the dataset
from sklearn.svm import SVR
regressor = SVR(kernel ='rbf')
regressor.fit(X, y)

# Visualising the SVR results
plt.scatter(sc_X.inverse_transform(X), sc_y.inverse_transform(y), color = 'red')
plt.plot(sc_X.inverse_transform(X), sc_y.inverse_transform(regressor.predict(X)), color = 'blue')
plt.title('Consumption of energy based on area(SVR)')
plt.xlabel('Area(m2)')
plt.ylabel('Consumption(KW/h)')
plt.show()

# Visualising the SVR results (for higher resolution and smoother curve)
X_grid = np.arange(min(X), max(X), 0.01) # choice of 0.01 instead of 0.1 step because the data is feature scaled
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(sc_X.inverse_transform(X),  sc_y.inverse_transform(y), color = 'red')
plt.plot(sc_X.inverse_transform(X_grid), sc_y.inverse_transform(regressor.predict(X_grid)), color = 'blue')
plt.title('Consumption of energy based on area (SVR)')
plt.xlabel('Area(m2)')
plt.ylabel('Consumption(KW/h)')
plt.show()