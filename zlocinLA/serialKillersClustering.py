# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math

def time(elem):
    elem = math.modf(elem/100)
    return (elem)[1]*60 + (elem)[0]*100
                              
# Importing the dataset
dataset = pd.read_csv('Crime_Data_from_2010.csv')
dataset.info()
dataset = dataset[dataset['Crime Code'] == 110]
dataset['Time Occurred'] = dataset['Time Occurred'].apply(time)
X = dataset.iloc[:, np.r_[3:5,9:14,15]]
X.info()