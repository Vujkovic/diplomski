# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import pandas as pd
import math
           
def time(elem):
    elem = math.modf(elem/100)
    return (elem)[1]*60 + (elem)[0]*100

# Importing the dataset
dataset = pd.read_csv('Crime_Data_from_2010.csv').tail(40000)

dataset['Date Occurred'] = pd.to_datetime(dataset['Date Occurred'])
dataset['Time Occurred'] = dataset['Time Occurred'].apply(time)

start_date = '01-01-2018'
end_date = '24-02-2018'

mask = (dataset['Date Occurred'] >= start_date) & (dataset['Date Occurred'] <= end_date)
T1 = ((dataset['Time Occurred'] > 0) & (dataset['Time Occurred'] <= 480)).value_counts()
T2 = ((dataset['Time Occurred'] > 480) & (dataset['Time Occurred'] <= 960)).value_counts()
T3 = ((dataset['Time Occurred'] > 960) & (dataset['Time Occurred'] <= 1440)).value_counts()

dataset = dataset.loc[mask]

ind = ["00:01 - 08:00", "08:01 - 16:00", "16:01 - 00:00"]
X = pd.Series([T1[1], T2[1], T3[1]], index = ind)
bx = X.plot(kind = 'bar')
bx.yaxis.set_major_formatter(mtick.PercentFormatter(29443))
plt.xticks(rotation=0)
plt.show()

numberOfCrimesByDistrict = dataset['Area Name'].value_counts()
ax = numberOfCrimesByDistrict.plot(kind = 'barh')
ax.xaxis.set_major_formatter(mtick.PercentFormatter(29443))
plt.show()